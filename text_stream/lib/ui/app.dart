import 'dart:html';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../stream/color_stream.dart';

class App extends StatelessWidget {
  @override 
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'asd',
      home: Scaffold(
        body: HomePageScreen(),
      ),
    );
  }
}

class HomePageScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget>{
  TextStream textStream = TextStream();
  final List<Text> msgList = [];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Color',
      home: Scaffold(
        appBar: AppBar(title: Text('App Bar'),),
        backgroundColor: Colors.white,
        body: ListView.separated(
          padding: const EdgeInsets.all(8),
          itemCount: msgList.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              height: 50,
              color: Colors.amber[600],
              child: Center(child: msgList[index]),
            );
          }, 
          separatorBuilder: (BuildContext context, int index) => const Divider(),
        ),
        floatingActionButton: FloatingActionButton(
          child: Text('C'),
          onPressed: () {
            changeText();
          },
        ),
      ),
    );
  }

  changeText() async {
    textStream.getText().listen((eventMsg) { 
      setState(() {
        print(eventMsg);
        msgList.add(eventMsg);
      });
    });
  }
}
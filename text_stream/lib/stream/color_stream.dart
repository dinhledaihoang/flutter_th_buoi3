import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextStream {
  Stream<Text> getText() async* {
    final List<Text> message = [
      Text('Nigredo'),
      Text('Albedo'),
      Text('Citrinitas'),
      Text('Rubedo'),
      Text('Magnum Opus'),
    ];

    yield* Stream.periodic(Duration(seconds: 1), (int t) {
      int index = t % 5;
      return message[index];
    });
  }
}